# What Is This?
This is a command line prepared php script for re-formating CSV file with table rates. See example

## input: tablerates_in.csv
| Country | Region/State | Zip/Postal Code | Weight | Shipping Price - Overnight| Shipping Price - 2nd Day |
| ------- | :----------: | :-------------: | :----: | :-----------------------: | -----------------------: |
| USA     | AL           |                 | 1-20   | 43                        | 42                       |
| USAR    | AL           |                 | 1-20   | 43                        | 42                       |
| USA     | ALR          |                 | 1-20   | 43                        | 42                       |
| USA     | AL           |                 | 1R-20  | 43                        | 42                       |
| USA     | AL           |                 | 21-40  | 66                        | 62                       |

## output: tablerates_out.csv
| Country | Region/State | Zip/Postal Code | # of Items (and above) | Shipping Price            |
| ------- | :----------: | :-------------: |:--------------------: | ------------------------: | 
| USA     | AL           | *               | 1                      | 43                        | 
| USA     | AL           | *               | 21                     | 62                        |

# Features
- Check country and region codes
- Check amount (is numeric)
- Define mapper mask and custom function for processing each item from input table (see code for more explanations)

# How to run
```sh
php file prepareTableRates.php --from-file=table_input.csv --to-file=table_out.csv --index=0
php file prepareTableRates.php -ftable_input.csv -ttable_out.csv -i0
```

# Index
```php
<?php
        static::$mappers[0] = array(
            ',,Info,,' => 'GWS TableRates 2016-06-10',
                ',,ExampleIn,,' => '
                Country,Region/State,Zip/Postal Code,Weight,Shipping Price - Overnight,Shipping Price - 2nd Day
                USA    ,AL          ,               ,1-20  ,43                        ,42
            ',
                ',,ExampleOut,,' => '
                Country,Region/State,"Zip/Postal Code","# of Items (and above)","Shipping Price"
                USA    ,AL          ,*                ,1                       ,42
            ',
            ',,Mapper,,' => array(
                'Country' => 'Country',
                'Region/State' => 'Region/State',
                'Zip/Postal Code' => 'Zip/Postal Code',
                'Weight' => '# of Items (and above)',
                'Shipping Price - 2nd Day' => 'Shipping Price'
            ),
            ',,Func,,' => $mapper0
        );
```